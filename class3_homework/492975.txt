I think the course needs more visualisations mainly in explaining the git branching as the default `git log` command is very unreadable and sometimes handwritten figures on whiteboard too. Something like this website is doing: https://learngitbranching.js.org
Looking forward for more advanced lessons as I knew the basics :)
